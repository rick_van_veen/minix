/* This file takes care of those system calls that deal with time.
 *
 * The entry points into this file are
 *   do_time:       perform the TIME system call
 *   do_stime:      perform the STIME system call
 *   do_times:      perform the TIMES system call
 */

#include "pm.h"
#include <minix/callnr.h>
#include <minix/com.h>
#include <signal.h>
#include "mproc.h"
#include "param.h"

/*===========================================================================*
 *              do_time                      *
 *===========================================================================*/
int do_time() {
    /* Perform the time(tp) system call. This returns the time in seconds since
     * 1.1.1970.  MINIX is an astrophysically naive system that assumes the earth
     * rotates at a constant rate and that such things as leap seconds do not
     * exist.
     */
    clock_t uptime, boottime;
    int s;

    if ( (s = getuptime2(&uptime, &boottime)) != OK)
        panic("do_time couldn't get uptime: %d", s);

    mp->mp_reply.reply_time = (time_t) (boottime + (uptime / system_hz));
    mp->mp_reply.reply_utime = (uptime % system_hz) * 1000000 / system_hz;
    return (OK);
}

/*===========================================================================*
 *        do_utctime             *
 *===========================================================================*/
int do_utctime() {
    clock_t uptime, boottime;
    time_t totSeconds;
    int s;

    time_t leapMoments[25] = {  78796740, 94694340, 126230340, 157766340, 189302340,
                                220924740, 252460740, 283996740, 315532740, 362793540,
                                394329540, 425865540, 489023940, 567993540, 631151940,
                                662687940, 709948740, 741484740, 773020740, 820454340,
                                867715140, 915148740, 1136073540, 1230767940, 1341100740
                             };

    if ( (s = getuptime2(&uptime, &boottime)) != OK)
        panic("do_time couldn't get uptime: %d", s);

    totSeconds = (time_t) (boottime + (uptime / system_hz));

    int i;
    for (i = 0; i < 25; ++i) {
        if (leapMoments[i] > totSeconds)
            break;
        totSeconds++;
    }
    mp->mp_reply.m_u.m_m2.m2l1 = totSeconds;
    return (OK);
}


/*===========================================================================*
 *              do_stime                     *
 *===========================================================================*/
int do_stime() {
    /* Perform the stime(tp) system call. Retrieve the system's uptime (ticks
     * since boot) and pass the new time in seconds at system boot to the kernel.
     */
    clock_t uptime, boottime;
    int s;

    if (mp->mp_effuid != SUPER_USER) {
        return (EPERM);
    }
    if ( (s = getuptime(&uptime)) != OK)
        panic("do_stime couldn't get uptime: %d", s);
    boottime = (long) m_in.stime - (uptime / system_hz);

    s = sys_stime(boottime);      /* Tell kernel about boottime */
    if (s != OK)
        panic("pm: sys_stime failed: %d", s);
    return (OK);
}

/*===========================================================================*
 *              do_times                     *
 *===========================================================================*/
int do_times() {
    /* Perform the times(buffer) system call. */
    register struct mproc *rmp = mp;
    clock_t user_time, sys_time, uptime;
    int s;

    if (OK != (s = sys_times(who_e, &user_time, &sys_time, &uptime, NULL)))
        panic("do_times couldn't get times: %d", s);
    rmp->mp_reply.reply_t1 = user_time;       /* user time */
    rmp->mp_reply.reply_t2 = sys_time;        /* system time */
    rmp->mp_reply.reply_t3 = rmp->mp_child_utime; /* child user time */
    rmp->mp_reply.reply_t4 = rmp->mp_child_stime; /* child system time */
    rmp->mp_reply.reply_t5 = uptime;      /* uptime since boot */

    return (OK);
}

