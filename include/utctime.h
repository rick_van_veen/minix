#include <lib.h>

int utctime(long *output) {
    message m;
    int res = _syscall(PM_PROC_NR, UTCTIME, &m);
    if (res == 0) *output = m.m_u.m_m2.m2l1;
    return (res);
}
